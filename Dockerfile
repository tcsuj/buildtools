FROM ubuntu:trusty

RUN apt-get update
RUN apt-get install -y texlive texlive-lang-polish texlive-latex-extra
RUN apt-get install -y python-setuptools git mercurial make gcc g++
RUN apt-get install -y nasm p7zip-full zip

ADD tools /tmp/buildtools
# Setuptools create a broken installation if called from any other directory,
RUN cd /tmp/buildtools && /usr/bin/python setup.py install
