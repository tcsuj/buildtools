Buildtools for the course materials repositories.

[![Build Status](https://drone.io/bitbucket.org/tcsuj/buildtools/status.png)](https://drone.io/bitbucket.org/tcsuj/buildtools/latest)

dobuild
-------

`dobuild` builds all Makefiles in a directory tree apart from the ones excluded with `.buildignore` files. Directories
that match the `.gitignore`-style patterns in the `.buildignore` files and all their subdirectories will be ignored
in the build process. All patterns are considered to be absolute (ie. `dir2` doesn't match `dir1/dir2`) and cannot be negated.

Usage:
    dobuild [optional make arguments]

Examples:

    dobuild
	dobuild -j5
	dobuild -n deploy
