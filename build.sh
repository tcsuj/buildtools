#!/bin/bash

set -eu

cd $(dirname $0)

TAG=
if [[ "${1:-}" == "--tag" ]]; then
	TAG="$(git tag --points-at HEAD)"
	if [[ -z "$TAG" ]]; then
		echo "No git tag points at current commit"
		exit 1
	fi
	if [[ "$(echo "$TAG" | wc -l)" -gt 1 ]]; then
		echo "More than one git tag points at current commit"
		exit 1
	fi
	TAG=":${TAG}"
fi

set -v
docker login -u tcsuj
docker build -t tcsuj/buildtools${TAG} .
docker push tcsuj/buildtools${TAG}
