import logging
import os
import subprocess
import sys
import buildtools.walker

def build(base, makeopts=[]):
    walker = buildtools.walker.Walker(base)
    if walker.errors:
        for e in walker.errors:
            logging.error('%s:%d: %s: %s', e[1], e[2], e[0], e[3].strip())
        return 1
    for d in walker.ignored:
        logging.info('Ignoring %s and subdirectories', d)
    for d in walker.accepted:
        make = subprocess.Popen(args = ['/usr/bin/make', '-w', '-C', d] + makeopts, cwd = base)
        returncode = make.wait()
        if returncode != 0:
            logging.error('make: exit code %d', returncode)
            if returncode < 0:
                return 2
            else:
                return returncode
    return 0

def main():
    logging.getLogger().setLevel(logging.INFO)
    return build('.', sys.argv[1:])

            
        
