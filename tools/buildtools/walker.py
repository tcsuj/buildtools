import os
import os.path
import pathspec

BUILDIGNORE = '.buildignore'
MAKEFILES = [ 'Makefile', 'makefile', 'GNUMakefile' ]

class Walker:
    def __init__(self, base):
        self.base = base
        self.patterns = []
        self.ignored = []
        self.accepted = []
        self.errors = []
        self.walk()
        if self.errors:
            pass
    
    def parse_buildignore(self, relative_path):
        file_path = os.path.join(relative_path, BUILDIGNORE)
        with open(os.path.join(self.base, file_path)) as f:
            for (lineno, line) in enumerate(f):
                """ We want to rebase these paths and ignore the distinction between paths starting with slash and others. """
                orig_line = line
                line = line.strip()
                if line == '':
                    continue
                if line.startswith('#'):
                    continue
                if line.startswith('/'):
                    line = line[1:]
                line = os.path.normpath(line)
                if line.startswith('../') or line == '..':
                    self.errors.append(('Pathspec refers to a file outside of the current directory', file_path, lineno, orig_line))
                    continue
                if line.startswith('!'):
                    self.errors.append(('Negative patterns are not supported', file_path, lineno, orig_line))
                    continue
                pattern = pathspec.GitIgnorePattern(os.path.normpath(os.path.join('/', relative_path, line)))
                assert pattern.include
                self.patterns.append(pattern)
    
    def walk(self):
        for (dirpath, dirnames, filenames) in os.walk(self.base):
            dirpath = os.path.relpath(dirpath, self.base)
            if BUILDIGNORE in filenames:
                self.parse_buildignore(dirpath)
            if any([list(pat.match([dirpath])) for pat in self.patterns]):
                self.ignored.append(dirpath)
                dirnames[:] = []
                continue
            if any([mkfile in filenames for mkfile in MAKEFILES]):
                self.accepted.append(dirpath)
        """ We want to build subdirectories first by default. """
        self.accepted.reverse()

