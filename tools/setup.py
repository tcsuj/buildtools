from setuptools import setup, find_packages

setup(
        name = 'BuildTools',
        version = '0.1',
        packages = find_packages(),
        install_requires = [
            'pathspec',
        ],
        entry_points = {
            'console_scripts': [
                'dobuild = buildtools.builder:main',
            ],
        },
        test_suite = 'tests',
)
