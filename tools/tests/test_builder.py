import buildtools.builder
import os.path

def test_fail1():
    returncode = buildtools.builder.build(os.path.join(os.path.dirname(__file__), 'fail1'))
    assert returncode == 1

def test_fail2():
    returncode = buildtools.builder.build(os.path.join(os.path.dirname(__file__), 'fail2'))
    assert returncode == 1

def test_good1():
    returncode = buildtools.builder.build(os.path.join(os.path.dirname(__file__), 'good1'))
    assert returncode == 0

def test_makefail():
    returncode = buildtools.builder.build(os.path.join(os.path.dirname(__file__), 'makefail'))
    assert returncode == 2
    
