import buildtools.walker
import os.path

def test_fail1():
    walker = buildtools.walker.Walker(os.path.join(os.path.dirname(__file__), 'fail1'))
    assert walker.errors

def test_fail2():
    walker = buildtools.walker.Walker(os.path.join(os.path.dirname(__file__), 'fail2'))
    assert walker.errors

def test_good1():
    walker = buildtools.walker.Walker(os.path.join(os.path.dirname(__file__), 'good1'))
    assert not walker.errors
    assert set(walker.ignored) == set(['b', 'a_', 'b_', 'a', 'c/subdir', 'c/subdir3'])
    """ Order matters here: we should have subdirs before superdirs. """
    assert set(walker.accepted) == set(['c_', 'c/subdir2', 'c'])
    assert list(filter(lambda x: x != 'c_', walker.accepted)) == ['c/subdir2', 'c']
